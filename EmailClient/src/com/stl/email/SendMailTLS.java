package com.stl.email;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMailTLS {

	public static void main(String[] args) {
		      // Recipient's email ID needs to be mentioned.
		      String to = "niranjan.jyotish@stlindia.com";//change accordingly

		      // Sender's email ID needs to be mentioned
		      //String from = "ranavir.dash@stlindia.com";//change accordingly
		      final String username = "ranavir.dash@stlindia.com";//change accordingly
		      final String password = "password";//change accordingly

		      // Assuming you are sending email through relay.jangosmtp.net
		      String host = "smtp.gmail.com";

		      Properties props = new Properties();
		      props.put("mail.smtp.auth", "true");
		      props.put("mail.smtp.starttls.enable", "true");
		      props.put("mail.smtp.host", host);
		      props.put("mail.smtp.port", "587");
		      props.put("mail.smtp.debug", "true");

		      // Get the Session object.
		      Session session = Session.getInstance(props,
		      new javax.mail.Authenticator() {
		         protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
		            return new javax.mail.PasswordAuthentication(username, password);
		         }
		      });
		      session.setDebug(true);

		      try {
		         // Create a default MimeMessage object.
		         Message message = new MimeMessage(session);

		         // Set From: header field of the header.
		         //message.setFrom(new InternetAddress(from));

		         // Set To: header field of the header.
		         message.setRecipients(Message.RecipientType.TO,
		         InternetAddress.parse(to));
		         // Set Subject: header field
		         message.setSubject("Testing Programatically sending email");
		       //Create HTML Body part
		         BodyPart messageBodyPart = new MimeBodyPart();
		         String html = "<html><head><title>" +
					        message.getSubject() +
				            "</title></head><body><div ><font color=\"#666666\"><b>Dear  Sir,</b></font></div> <br/>" +
				            "<div style=\"color:blue;\">"+"Testing Email with added style and file attachement"+"</div> <br/><br/><br/>"+
				            "<div><font color=\"#666666\"> Thanks & Regards</font></div><br/>"+"<div><font color=\"#666666\"> Ranavir Dash</font></div>"+
				            "</body></html>";
		       //Set the HTML body to the message with DataHandler
		         messageBodyPart.setDataHandler(new DataHandler(new HTMLDataSource(html)));
		       //Use Multipart for file attachment along with Body Text
		         Multipart multipart = new MimeMultipart();
		         multipart.addBodyPart(messageBodyPart);
		         //Again create a new body part for attachments
		         messageBodyPart = new MimeBodyPart();
		         String filename = "HelloWorld.apk";
		         DataSource source = new FileDataSource(filename);
		         messageBodyPart.setDataHandler(new DataHandler(source));
		         messageBodyPart.setFileName(filename);
		         multipart.addBodyPart(messageBodyPart);

		         // Send the complete message parts
		         message.setContent(multipart);
		         
		         /*// Set Subject: header field
		         message.setSubject("Testing Programatically sending email");
		         //Create HTML Body part
		         String html = "<html><head><title>" +
					        message.getSubject() +
				            "</title></head><body><div ><font color=\"#666666\"><b>Dear  Sir</b></font></div> <br/>" +
				            "<div style=\"color:blue;\">"+"Testing Email with added style and file attachement"+"</div> <br/><br/><br/>"+
				            "<div><font color=\"#666666\"> Thanks & Regards</font></div><br/>"+"<div><font color=\"#666666\"> Ranavir Dash</font></div>"+
				            "</body></html>";
		         //Set the HTML body to the message with DataHandler
		         message.setDataHandler(new DataHandler(new HTMLDataSource(html)));
		         //Use Multipart for file attachment
		         Multipart multipart = new MimeMultipart();
		         
		         addAtachments(new String[]{"HelloWorld.apk"}, multipart);
		         message.setContent(multipart);*/
		         //
		      // Create the message part
		         //BodyPart messageBodyPart = new MimeBodyPart();

		         // Now set the actual message
		        // messageBodyPart.setText("This is message body");

		         // Create a multipar message
		        // Multipart multipart = new MimeMultipart();

		         // Set text message part
		        // multipart.addBodyPart(messageBodyPart);

		         // Part two is attachment
		         
		         //
		         /**********************************/
		         
			       /*// msg.setContent(message, "text/plain");
			        
			        //use a MimeMultipart as we need to handle the file attachments
			        Multipart multipart = new MimeMultipart();        
			        //add the message body to the mime message
			        BodyPart messageBodyPart = new MimeBodyPart();
			        //messageBodyPart.setText("Dear Sir,\n"
			        //+" 			This is a Testing email so ignore it...\n\n"+"Thanks & Regards\nRanavir Dash");
			        
			        multipart.addBodyPart(messageBodyPart);        
			        // add any file attachments to the message
			        //addAtachments(attachments, multipart);
			        
			        // Put all message parts in the message
			        message.setContent(multipart);
			        */
			       
			       /* 
			        String html = "<html><head><title>" +
			        message.getSubject() +
		            "</title></head><body><div ><font color=\"#666666\"><b>Hello  "+"Sir"+"</b></font></div> <br/>" +
		            "<div>"+"Testing Email"+"</div> <br/><br/><br/>"+
		            "<div><font color=\"#666666\"> Thanks</font></div><br/>"+"<div><font color=\"#666666\"> Evaluat Admin for research oppo.</font></div>"+
		            "</body></html>";

			        // HTMLDataSource is an inner class
			        message.setDataHandler(new DataHandler(new HTMLDataSource(html)));*/
		         /**********************************/
		         // Send message
		         Transport.send(message);

		         System.out.println("Sent message successfully....");

		      } catch (MessagingException e) {
		            throw new RuntimeException(e);
		      }
	}
		      //inner class
		      static class HTMLDataSource implements DataSource {
		    	    private String html;

		    	    public HTMLDataSource(String htmlString) {
		    	        html = htmlString;
		    	    }

		    	    // Return html string in an InputStream.
		    	    // A new stream must be returned each time.
		    	    public InputStream getInputStream() throws IOException {
		    	        if (html == null) throw new IOException("Null HTML");
		    	        return new ByteArrayInputStream(html.getBytes());
		    	    }

		    	    public OutputStream getOutputStream() throws IOException {
		    	        throw new IOException("This DataHandler cannot write HTML");
		    	    }

		    	    public String getContentType() {
		    	        return "text/html";
		    	    }

		    	    public String getName() {
		    	        return "JAF text/html dataSource to send e-mail only";
		    	    }
		    	}
		      /*protected static void addAtachments(String[] attachments, Multipart multipart)throws MessagingException, AddressException{
		    		  for(int i = 0; i<= attachments.length -1; i++)
		    		  {
		    		  String filename = attachments[i];
		    		  MimeBodyPart attachmentBodyPart = new MimeBodyPart();

		    		  //use a JAF FileDataSource as it does MIME type detection
		    		  DataSource source = new FileDataSource(filename);
		    		  attachmentBodyPart.setDataHandler(new DataHandler(source));

		    		  //assume that the filename you want to send is the same as the
		    		  //actual file name - could alter this to remove the file path
		    		  attachmentBodyPart.setFileName(filename);

		    		  //add the attachment
		    		  multipart.addBodyPart(attachmentBodyPart);
		    		  }
		      }*/
		/*final String username = "captain1888";//username@gmail.com
		final String password = "rantest123";//password

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "relay.jangosmtp.net");
		props.put("mail.smtp.port", "25");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(username, password) ;
			}
		  });

		try {

			Message message = new MimeMessage(session);
			//message.setFrom(new InternetAddress("captain1888@gmail.com"));//from-email@gmail.com
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("ranavir.silicon@yahoo.co.in"));//to-email@gmail.com
			message.setSubject("Testing Subject");
			message.setText("Dear Mail Crawler,"
				+ "\n\n No spam to my email, please!");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}*/
	
}



